CREATE TABLE workFlowConfig (
  uuid varchar(36),
  id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  workflow varchar(MAX),
  name varchar(100)
);
