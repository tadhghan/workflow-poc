$(document).ready(function(){

  function workFlowBuilder(config){
    console.log(config);
    var blockCount = 0;
    $.each(config, function(key, block){
      var blockID   = "block-" + blockCount;
      var blockAttr = {'id': blockID, 'class':'wfc-container wfc-block block-shadow'};
      $('<div/>', blockAttr).appendTo('#work-flow');

      var attributes = Object.assign({}, block);
      delete attributes.sections;
      createBlockHead(attributes, blockCount);
      createBlockSections(block.sections, blockCount);
      createBodySection(block.sections, blockCount);
    })
    blockCount++;
  }

  function workFlowReader(){

  }

  function changeSection(newSection){
    console.log(newSection);
  }

  function createBlockHead(attributes, blockCount){
    var blockHead       = {'id': '', 'class': 'wfc-block-head vertical-padding', 'text': ''};
    var blockHeadIcon   = {'id': '', 'class': 'wfc-block-type', 'text': '', 'icon': 'iconType'};
    var blockHeadTitle  = {'id': '', 'class': 'wfc-block-title', 'text': attributes.name};
    var blockHeadExpand = {'id': '', 'class': 'wfc-block-expand', 'text': '', 'icon': 'iconExpand'};
    var iconType        = {'id': '', 'class': 'material-icons', 'text': attributes.type.toLowerCase()};
    var iconExpand      = {'id': '', 'class': 'material-icons', 'text': 'expand_more'};
    var icons           = {'iconType': iconType, 'iconExpand': iconExpand};
    var elements        = [blockHeadIcon, blockHeadTitle, blockHeadExpand];
    var thisBlock       = '#block-' + blockCount;

    $('<div/>', blockHead).appendTo(thisBlock);
    thisBlock += ' .wfc-block-head';
    for(var i=0; i<elements.length; i++) {
      $('<div/>', elements[i]).appendTo(thisBlock);

      if(elements[i].hasOwnProperty('icon'))
        $('<div/>', icons[''+elements[i].icon]).appendTo(thisBlock+' .'+elements[i].class);
    }

  }

  function createBlockSections(sections, blockCount){
    var blockSection    = {'id': '', 'class': 'wfc-block-sections vertical-padding', 'text': ''};
    var iconExpand      = {'id': '', 'class': 'material-icons', 'text': 'event_note'};
    var thisBlock       = '#block-' + blockCount;
    var firstTime       = true;
    $('<div/>', blockSection).appendTo(thisBlock);
    //console.log(sections);

    thisBlock += ' .wfc-block-sections';
    for (var i = 0; i < sections.length; i++) {
      var sectionID           = 'block-circle-'+i;
      var connectorID         = 'connector'+i;
      var sectionCircleClass  = '';
      var iconConnectText     = '';

      if(firstTime == true) { var sectionCircleClass = 'wfc-block-circle left first';}
      else { var sectionCircleClass = 'wfc-block-circle left';}

      if(sections[i].movement == 'to') { var iconConnectText = 'chevron_right'; }
      else { var iconConnectText = 'code'; }

      var sectionConnector    = {'id': connectorID, 'class': 'wfc-block-connector left'};
      var sectionCircle       = {'id': sectionID, 'class': sectionCircleClass};
      var iconConnect         = {'id': '', 'class': 'material-icons', 'text': iconConnectText};

      $('<div/>', sectionCircle).appendTo(thisBlock);
      $('<div/>', iconExpand).appendTo(thisBlock+' #'+sectionID+'.wfc-block-circle');

      $('<div/>', sectionConnector).appendTo(thisBlock);
      $('<div/>', iconConnect).appendTo(thisBlock+' #'+connectorID+'.wfc-block-connector');
      firstTime = false;
    }
    MyApp.sections = sections;
  }

  function createBodySection(section, blockCount){
    var thisBlock = '#block-' + blockCount;
    var bodyAttr  = {'id': 'block-body', 'class':'wfc-block-body vertical-padding'};
    $('<div/>', bodyAttr).appendTo(thisBlock);
    console.log();
    //$("#block-body").loadTemplate("view/templates/section-body.html", section[0], {bindingOptions: {"ignoreUndefined": true, "ignoreNull": true, "ignoreEmptyString": true}});
    //Cut up object remove any keys you wont use then load templates based on available keys.
    //Have them share a similar name
  }

  MyApp.workFlowBuilder = workFlowBuilder;
  MyApp.workFlowReader  = workFlowReader;
  MyApp.changeSection   = changeSection;
});
