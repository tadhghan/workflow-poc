$(document).ready(function(){

  var blocks = [];
  $('#htmlToJson').children().each(function(){
    var b = {type: $(this).attr('data-type'), name: $(this).attr('data-name'), bid: "1234"};
    blocks.push(b);
  })

  var workFlowConfig = JSON.stringify(blocks);
  //postBlocks(workFlowConfig);
  //getBlocks("workFlowOne");
  getConfig();

  $('.wfc-block-circle').on('click', function(){
    var sectionID = $(this).attr('id');
    MyApp.changeSection(sectionID[sectionID.length-1]);
  });

  //seperate functions are more efficient with AJAX
  //1 function call overrides previous one
  //Data gets sent but the request can't be heard
  function postBlocks(workFlowConfig) {
    $.ajax({
      type: "POST",
      url: "/sendBlocks",
      timeout: 2000,
      data: {"sendBlocks": workFlowConfig},
      success: function(data) {
          console.log(data);
      },
      error: function(jqXHR, textStatus, err) {
          console.log("POST" + jqXHR + ":" + textStatus + ":" + err);
      }
    });
  }

  function getBlocks(workFlowName) {
    $.ajax({
      type: "GET",
      url: "/getBlocks",
      timeout: 2000,
      data: {"getBlocks": workFlowName},
      success: function(data) {
          var workFlow = JSON.parse(data);
          console.log(workFlow.recordset[0].uuid);
      },
      error: function(jqXHR, textStatus, err) {
          console.log("GET" + jqXHR + ":" + textStatus + ":" + err);
      }
    });
  }

  function getConfig() {
    $.ajax({
      type: "GET",
      url: "/testObject",
      timeout: 2000,
      data: {"getBlocks": "blah"},
      success: function(data) {
          var workFlowConfig = JSON.parse(data);
          MyApp.workFlowBuilder(workFlowConfig);
      },
      error: function(jqXHR, textStatus, err) {
          console.log("GET" + jqXHR + ":" + textStatus + ":" + err);
      }
    });
  }

});
