var express = require('express');
var app = express();
var msSql = require('./msSql.js');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var testWFC = '[{"type":"application","name":"GY666 - Application Form","bid":"1234"},{"type":"assessment","name":"GY666 - Assessment","bid":"1234"}]';
var testConfig = ' [{ "type": "assessment", "name":"GY666 - Assessment", "id":"1234", "sections": [ { "name": "First Section", "instructions": ["Kick the can down the line", "Do a little dance"], "movement": "to", "icon": "assess", "eu": "eu", "internal-note": "yes", "externalNote": "yes", "deposit-paid": "no", "documents": "no", "group": ["super-admin", "Can kickers"], "triggers": [] }, { "name": "Second Section", "instructions": "Kick it further down", "movement": "toAndFro", "icon": "assess", "eu": "eu", "internal-note": "yes", "deposit-paid": "no", "documents": "no" }, { "name": "Third Section", "instructions": "Kick the can into class", "movement": "to", "icon": "assess", "eu": "eu", "internal-note": "yes", "deposit-paid": "no", "documents": "no" } ] }]';

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}))

app.get('/index', function (req, res) {
   res.sendFile( __dirname + "/public/view/index.html");
})

app.post('/sendBlocks', function(req, res){
  //console.log(req.body.sendBlocks);
  res.send('Workflow Sent');
})

app.get('/getBlocks', function(req, res){
  var queryString = "SELECT * FROM workFlowConfig WHERE name = '" + req.query.getBlocks + "'";
  console.log(queryString);
  msSql.query(queryString, req, res);
})

app.get('/testsql', function (req, res) {
  msSql.query("SELECT * FROM PAC_COURSE_GROUP", req, res);
})

app.get('/testObject', function(req, res){
  res.send(testConfig);
})

app.get('/jsonTest', function(req, res){
  res.sendFile(__dirname + "/public/js/config.json");
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})
